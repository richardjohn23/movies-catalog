

# MoviesCatalog

Display list of items from an iTunes Search API with a detailed view for each item

## Installing / Getting started

At the root of the project, run:

```pod install
```

These will install libraries and frameworks used in the project

### Initial Configuration

Go to the project settings in the Build Settings tab, make sure to select Pods in your target 
and navigate to CocoaLumberjack in the selection and change 'Swift Version' to Swift 4.2 in the search bar

## Features

* This project uses the Clean code architecture
* The benefits of using this architecture are as follows
    1. Easier to test, easy to read, easy to modify
    2. Bugs are more easier to detect
    3. Avoids spaghetti code at some point
