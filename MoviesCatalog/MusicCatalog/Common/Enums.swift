//
//  Enums.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public enum TaskError: Error {
    case Saving(String)
    case Fetching(String)
}
