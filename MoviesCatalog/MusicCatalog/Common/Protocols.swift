//
//  Protocols.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Result
import RxCocoa
import RxSwift

public typealias ProductRequestCompletionBlock = (Result<[Product], TaskError>) -> ()
public typealias ProductFetchCompletionBlock = (Result<[ProductProtocol], TaskError>) -> ()
//public typealias DisplayProductsCompletionBlock = (Result<[ProductProtocol], TaskError>) -> ()

public protocol UserProtocol {

}

public protocol ViewControllerProtocol {
    var appController: AppControllerProtocol! { get set }
}

public protocol APIFetchProtocol {
    func fetchRemoteProducts(completion: @escaping ProductRequestCompletionBlock)
}

public protocol APIProtocol: APIFetchProtocol {
    func initialize()
}

public protocol DataStoreProtocol {
    func fetchProducts(completion: @escaping ProductFetchCompletionBlock)
    
}
