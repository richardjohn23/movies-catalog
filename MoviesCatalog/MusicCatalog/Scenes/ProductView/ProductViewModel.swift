//
//  ProductViewModel.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductViewModelProtocol {
    var name: String { get set }
    var price: String { get set }
    var genre: String { get set }
    var image: URL? { get set }
    var description: String { get set }
}

public struct ProductViewModel: ProductViewModelProtocol {
    public var name: String
    public var price: String
    public var genre: String
    public var image: URL? = nil
    public var description: String
    
    init(name: String, price: String, genre: String, description: String) {
        self.name = name
        self.price = price
        self.genre = genre
        self.description = description
    }

}
