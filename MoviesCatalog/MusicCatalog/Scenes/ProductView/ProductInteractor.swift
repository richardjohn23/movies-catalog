//
//  ProductInteractor.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductsViewRoutingLogic
{
    func routeToProductDetail()
}

public protocol ProductsViewBusinessLogic
{
    var appController: AppControllerProtocol! { get set }
    func displayProducts()
    func selected(productAtIndexPath: IndexPath)
}

class ProductInteractor: ProductsViewBusinessLogic {
    var appController: AppControllerProtocol!
    
    var products = [ProductProtocol]()
    
    public var dataTransporter: AnyDataTransport<ProductProtocol, ProductViewModelProtocol>?
    
    public var presenter: ProductsViewPresentationLogic?
    public var worker: ProductsWorkerProtocol?
    
    public init(aWorker: ProductsWorkerProtocol) {
        worker = aWorker
    }
    
    func displayProducts() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.fetchProducts()
        }
    }
    
    func fetchProducts() {
        self.worker?.fetchProducts(completion: { (result) in
            switch result {
            case let .success(products):
                self.products = products
                self.presenter?.presentProducts(products: products)
            case let .failure(TaskError.Fetching(desc)):
                self.presenter?.presentError(errorDesc: desc)
            case let .failure(TaskError.Saving(desc)):
                self.presenter?.presentError(errorDesc: desc)
            }
        })
    }
    
    public func selected(productAtIndexPath indexPath: IndexPath) {
        let selectedProduct = products[indexPath.row]
        var productViewModel = ProductViewModel(name: selectedProduct.name, price: "\(selectedProduct.price)", genre: selectedProduct.genre, description: selectedProduct.description)
        productViewModel.image = selectedProduct.images?.first?.url()
        
        self.dataTransporter?.inbound(data: selectedProduct)
        self.dataTransporter?.inbound(viewModel: productViewModel)
    }
    
}
