//
//  ProductWorker.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductsWorkerProtocol {
    var productAppInterface: AppProductManagementProtocol? { get set }
    func fetchProducts(completion: @escaping ProductFetchCompletionBlock)
}

class ProductWorker: ProductsWorkerProtocol {
    
    public var productAppInterface: AppProductManagementProtocol?
    
    //fetch from local datastore
    public func fetchProducts(completion: @escaping ProductFetchCompletionBlock) {
        productAppInterface?.fetchProducts(completion: completion)
    }
    
    public init() {
        
    }

}
