//
//  ProductsPresenter.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductsViewDisplayLogic: class {
    func reloadProducts(products: [ProductViewModel])
}

public protocol ProductsViewPresentationLogic {
    func presentProducts(products: [ProductProtocol])
    func presentError(errorDesc: String)
}

public class ProductsPresenter: ProductsViewPresentationLogic {
    public weak var viewController: (ProductsViewDisplayLogic)?
    
    public init(){}

    public func presentProducts(products: [ProductProtocol]) {
        //        viewController?.inboundData(data: products)
        
        DispatchQueue.main.async {
            var productViewModels = [ProductViewModel]()
            
            productViewModels = products.map({ (product) -> ProductViewModel in
                var prod = ProductViewModel(name: product.name, price: product.price.format(f: ".1"), genre: product.genre, description: product.description )
                prod.image = product.images!.first!.url()
                return prod
            })
            self.viewController?.reloadProducts(products: productViewModels)
        }
        
    }
    
    public func presentError(errorDesc: String) {
        print("Error: \(errorDesc)")
    }
}

