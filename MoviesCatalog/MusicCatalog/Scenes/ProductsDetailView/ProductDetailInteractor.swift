//
//  ProductDetailInteractor.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 07/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductDetailBusinessLogic {
    var appController: AppControllerProtocol! { get set }
    func loadSelectedProduct()
}

class ProductDetailInteractor: NSObject, ProductDetailBusinessLogic, DataTransportProtocol {
    
    public var appController: AppControllerProtocol!
    
    public var presenter: ProductDetailPresentationLogic?
    var receivedViewModel: ProductViewModelProtocol!
    var receivedData: ProductProtocol!
    
    public typealias ViewModel = ProductViewModelProtocol
    public typealias DataModel = ProductProtocol
    
    public func inbound(viewModel: ProductViewModelProtocol) {
        self.receivedViewModel = viewModel
    }
    
    public func inbound(data: ProductProtocol) {
        self.receivedData = data
    }
    
    public func outboundData() -> ProductProtocol? {
        return self.receivedData
    }
    
    public func outboundViewModel() -> ProductViewModelProtocol? {
        return self.receivedViewModel
    }
    
    public func loadSelectedProduct() {
        self.presenter?.presentProduct(product: self.receivedViewModel)
    }

}
