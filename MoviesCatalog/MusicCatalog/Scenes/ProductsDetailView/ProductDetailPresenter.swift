//
//  ProductDetailPresenter.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 07/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductDetailDisplayLogic: class {
    func displayProduct(count: String)
    func display(product: ProductViewModelProtocol)
}

public protocol ProductDetailPresentationLogic {
    func presentProduct(product: ProductViewModelProtocol)
}

public class ProductDetailPresenter: NSObject, ProductDetailPresentationLogic {

    public weak var viewController: ProductDetailDisplayLogic?
    
    public func presentProduct(product: ProductViewModelProtocol) {
        viewController?.display(product: product)
    }
}
