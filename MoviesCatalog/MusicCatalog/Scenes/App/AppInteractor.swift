//
//  AppInteractor.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Result

public protocol AppRoutingLogic {
    var controller:AppControllerProtocol! { get set }
    func moveToProducts()
}

public protocol AppControllerProtocol {
    var presenter: AppPresentationLogic!  { get set }
    var router: AppRoutingLogic! { get set }
    var interactor: AppBusinessLogic! { get set }
    
    func didLaunch()
    func restAPI()
}

public protocol AppRemoteProductManagementProtocol {
    func fetchRemoteProducts(completion:  @escaping ProductRequestCompletionBlock)
}

public protocol AppProductManagementProtocol {
    func fetchProducts(completion:  @escaping ProductFetchCompletionBlock)
}

public protocol AppBusinessLogic: AppProductManagementProtocol, AppRemoteProductManagementProtocol {
    
    var restAPI: APIProtocol! { get set }
    func initializeServices()
    func dataStore() -> DataStoreProtocol
}

public class AppInteractor: AppBusinessLogic {

    public var presenter: AppPresentationLogic!
    public var restAPI: APIProtocol!
    private var aDataStore: DataStoreProtocol!
    
    public init(dataStore: DataStoreProtocol) {
        self.aDataStore = dataStore
    }
    
    public func initializeServices() {
        restAPI.initialize()
    }
    
    public func dataStore() -> DataStoreProtocol {
        return aDataStore
    }
}

// MARK: User session tasks
extension AppInteractor {
    public func fetchProducts(completion: @escaping ProductFetchCompletionBlock) {
        fetchRemoteProducts { (result) in
            switch result {
            case .success(let data):
                self.dataStore().fetchProducts(completion: completion)
                print(data)
            case let .failure(error):
                completion(Result.failure(error))
            }
        }
    }
}

// MARK: Request and Result of REMOTE products Tasks
extension AppInteractor {
    public func fetchRemoteProducts(completion:@escaping ProductRequestCompletionBlock) {
        restAPI.fetchRemoteProducts(completion: completion)
    }
}
