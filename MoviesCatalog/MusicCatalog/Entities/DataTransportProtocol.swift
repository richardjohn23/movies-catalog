//
//  DataTransportProtocol.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 07/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol DataTransportProtocol {
    
    associatedtype DataModel
    associatedtype ViewModel
    
    func inbound(data: DataModel)
    func outboundData() -> DataModel?
    
    func outboundViewModel() -> ViewModel?
    func inbound(viewModel: ViewModel)
}

private class _AnyDataModelBase<DataModel, ViewModel>: DataTransportProtocol {
    
    init() {
        guard type(of: self) != _AnyDataModelBase.self else {
            fatalError("_AnyViewModel<ViewModel> instances can not be created; create a subclass instance instead")
        }
    }
    
    func outboundData() -> DataModel? {
        fatalError("Must override")
    }
    
    func inbound(data: DataModel) {
        fatalError("Must override")
    }
    
    func outboundViewModel() -> ViewModel? {
        fatalError("Must override")
    }
    
    func inbound(viewModel: ViewModel) {
        fatalError("Must override")
    }
}

private final class _AnyDataModelBox<Concrete: DataTransportProtocol>: _AnyDataModelBase<Concrete.DataModel, Concrete.ViewModel> {
    var concrete: Concrete
    
    init(_ concrete: Concrete) {
        self.concrete = concrete
    }
    
    override func outboundData() -> Concrete.DataModel? {
        return concrete.outboundData()!
    }
    
    override func inbound(data: Concrete.DataModel) {
        concrete.inbound(data: data)
    }
    
    override func outboundViewModel() -> ViewModel? {
        return concrete.outboundViewModel()
    }
    
    override func inbound(viewModel: ViewModel) {
        concrete.inbound(viewModel: viewModel)
    }
}

public class AnyDataTransport<DataModel, ViewModel>: DataTransportProtocol {
    
    private let box: _AnyDataModelBase<DataModel, ViewModel>
    
    public init<Concrete: DataTransportProtocol>(_ concrete: Concrete) where Concrete.DataModel == DataModel, Concrete.ViewModel == ViewModel {
        box = _AnyDataModelBox(concrete)
    }
    
    public func outboundData() -> DataModel? {
        return box.outboundData()
    }
    
    public func inbound(data: DataModel) {
        box.inbound(data: data)
    }
    
    public func outboundViewModel() -> ViewModel? {
        return box.outboundViewModel()
    }
    
    public func inbound(viewModel: ViewModel) {
        box.inbound(viewModel: viewModel)
    }
}

