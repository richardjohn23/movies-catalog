//
//  Product.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductProtocol {
    var name: String { get set }
    var price: Double { get set }
    var images: Array<ProductImageProtocol>? { get set }
    var genre: String { get set }
    var description: String { get set }
}

public struct Product: ProductProtocol {
    public var name: String
    public var price: Double
    public var images: Array<ProductImageProtocol>?
    public var genre: String
    public var description: String
    
    public init(name: String, price: Double, genre: String, description: String) {
        self.name = name
        self.price = price
        self.genre = genre
        self.description = description
    }
}
