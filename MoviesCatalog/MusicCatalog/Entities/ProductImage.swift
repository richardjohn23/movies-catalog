//
//  ProductImage.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

public protocol ProductImageProtocol {
    var stringURL: String { get set }
    func url() -> URL
}

// MARK: Image Protocol

extension ProductImageProtocol {
    public func url() -> URL {
        return URL(string: self.stringURL)!
    }
}

public struct ProductImage: ProductImageProtocol {
    public var stringURL: String
    
    public init(stringUrl strURL: String) {
        self.stringURL = strURL
    }
}


