//
//  ProductsViewController.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Kingfisher

class ProductsViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ProductsViewDisplayLogic {
    
    var interactor: ProductsViewBusinessLogic!
    
    let screenSize = UIScreen.main.bounds
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var productViewModels: [ProductViewModel]!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

        
        productViewModels = [ProductViewModel]()
        interactor.displayProducts()
    }
}

extension ProductsViewController {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenSize.width - 16, height: 120);
    }
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productViewModels.count;
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        let cellModel = productViewModels[indexPath.row]
        cell.productNameLabel.text = cellModel.name
        cell.productPriceLabel.text = cellModel.price
        cell.productGenreLabel.text = cellModel.genre
//        cell.imageView.kf.setImage(with: cellModel.image)
        cell.imageView.kf.setImage(with: cellModel.image, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        return cell;
       }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        interactor.selected(productAtIndexPath: indexPath)
        performSegue(withIdentifier: "ProductDetail", sender: nil)
    }
}
    
extension ProductsViewController {
    func reloadProducts(products: [ProductViewModel]) {
        self.productViewModels = products
        self.collectionView.reloadData()
    }
}


