//
//  ProductDetailViewCell.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 07/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

class ProductDetailViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
