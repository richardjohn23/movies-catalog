//
//  ProductDetailViewController.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductDetailViewController: BaseViewController, UITableViewDelegate, ProductDetailDisplayLogic {
    func displayProduct(count: String) {
        
    }
    var disposeBag = DisposeBag()
    
    var interactor: ProductDetailBusinessLogic? {
        didSet {
            
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel: ProductViewModelProtocol?
    var router: ProductDetailRoutingLogic?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.loadSelectedProduct()
    }
}
    
extension ProductDetailViewController {
    func display(product: ProductViewModelProtocol) {
        let models = [product]
            
        let items = Observable.just(models)
            
        items
            .bind(to: tableView.rx.items(cellIdentifier: "DetailViewCell", cellType: ProductDetailViewCell.self)) { (row, element, cell) in
                let model = element
                cell.productNameLabel.text = ("Track name: \(model.name.uppercased(with: nil))")
                cell.productPriceLabel.text = ("Price: \(model.price)")
                cell.productImageView.kf.setImage(with: model.image)
                cell.productDescription.text = model.description
            }
            .disposed(by: disposeBag)
    }
}
    

