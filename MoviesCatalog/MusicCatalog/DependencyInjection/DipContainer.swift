//
//  DipContainer.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Dip

extension UIViewController {
    enum Tags {
        static let productsView = "productsView"
        static let productDetailView = "productDetailView"
    }
}

extension AppDelegate {
    func setupDIP(container: DependencyContainer) {
        
        let dependencyFactory = DependencyFactory()
        
        container.register(.singleton) {
            ProductDetailInteractor() as ProductDetailInteractor
        }
        
        container.register(.singleton) { AppController() as AppControllerProtocol }
            .resolvingProperties { (container, controller) in
//                let appWorker = AppWorker(abstractWorker: dependencyFactory.user())
                var appController = controller
                let interactor = AppInteractor(dataStore: dependencyFactory.dataStore())
                let presenter = AppPresenter()
                let router = AppRouter()
                
                router.window = self.window!
                router.storyboard = self.mainStoryboard
                router.controller = controller
                appController.router = router
                appController.interactor = interactor
                presenter.controller = appController
                interactor.presenter = presenter
                interactor.restAPI = dependencyFactory.remoteAPI()
        }
        
      
        
        container.register(tag: UIViewController.Tags.productsView) { ProductsViewController() }
            .resolvingProperties { (container, controller) in
                let appController = try! container.resolve() as AppControllerProtocol
                controller.appController = appController
                controller.title = NSLocalizedString("Products", comment: "product title");
                
                let productsWorker = ProductWorker()
                let viewController = controller
                let interactor = ProductInteractor(aWorker: productsWorker)
                let presenter = ProductsPresenter()
                let router = ProductsRouter()
                
                productsWorker.productAppInterface = controller.appController.interactor
                viewController.interactor = interactor
                interactor.presenter = presenter
                interactor.worker = productsWorker
                interactor.appController = appController
                interactor.dataTransporter = AnyDataTransport(try! container.resolve() as ProductDetailInteractor)
                presenter.viewController = viewController
                router.viewController = viewController
        }
        
        container.register(tag: UIViewController.Tags.productDetailView) { ProductDetailViewController() }
            .resolvingProperties { (container, controller) in
                let appController = try! container.resolve() as AppControllerProtocol
                let interactor = try! container.resolve() as ProductDetailInteractor
                let presenter = ProductDetailPresenter()
                let router = ProductDetailRouter()
                
                controller.appController = try! container.resolve() as AppControllerProtocol
                controller.interactor = interactor
                controller.router = router
                interactor.appController = appController
                interactor.presenter = presenter
                presenter.viewController = controller
                router.viewController = controller
        }
        DependencyContainer.uiContainers = [container]
    }
}



