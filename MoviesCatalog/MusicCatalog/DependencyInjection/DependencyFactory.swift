//
//  DependencyFactory.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

protocol DependencyFactoryProtocol {
    func remoteAPI () -> APIProtocol
    func dataStore () -> DataStoreProtocol
}

struct DependencyFactory: DependencyFactoryProtocol {
    
    var env = Configuration.sharedInstance.environment {
        didSet {
            
        }
    }
    
    func remoteAPI() -> APIProtocol {
        print("current env: \(env)")
        if env == Environment.Staging {
            return FakeRemoteAPI()
        }else{
            return RemoteAPI()
        }
    }
    
    func dataStore() -> DataStoreProtocol {
        if env == Environment.Staging {
            return FakeDataStore()
        }else{
            return DataStore()
        }
    }
    
    func user() -> UserProtocol {
        if env == Environment.Staging {
            return FakeUser.sharedInstance
        }else{
            return MyUser.sharedInstance
        }
    }
}
