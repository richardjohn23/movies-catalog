//
//  FakeDataStore.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Result

class FakeDataStore: NSObject, DataStoreProtocol {
    
    
    func fetchProducts(completion: @escaping ProductFetchCompletionBlock) {
        let image = ProductImage(stringUrl: "https://is4-ssl.mzstatic.com/image/thumb/Music118/v4/c8/9a/6f/c89a6ff4-5e67-ddd3-db24-2fce7868ba59/source/100x100bb.jpg")
        var product = Product(name: "Star", price: 1.29, genre: "Pop", description: "Experience the heroic action and unforgettable adventures of Star Wars: Episode I - The Phantom Menace.  See the first fateful steps in the journey of Anakin Skywalker. Stranded on the desert planet Tatooine after rescuing young Queen Amidala from the impending invasion of Naboo, Jedi apprentice Obi-Wan Kenobi and his Jedi Master Qui-Gon Jinn discover nine-year-old Anakin, a young slave unusually strong in the Force. Anakin wins a thrilling Podrace and with it his freedom as he leaves his home to be trained as a Jedi. The heroes return to Naboo where Anakin and the Queen face massive invasion forces while the two Jedi contend with a deadly foe named Darth Maul. Only then do they realize the invasion is merely the first step in a sinister scheme by the re-emergent forces of darkness known as the Sith.")
        product.images = [image]

        
        var product2 = Product(name: "Star Wars", price: 19.99, genre: "Action & Adventure", description: "Discover the conflict between good and evil in the electrifying Star Wars: Episode V - The Empire Strikes Back. After the destruction of the Death Star, Imperial forces continue to pursue the Rebels. After the Rebellion’s defeat on the ice planet Hoth, Luke journeys to the planet Dagobah to train with Jedi Master Yoda, who has lived in hiding since the fall of the Republic. In an attempt to convert Luke to the dark side, Darth Vader lures young Skywalker into a trap in the Cloud City of Bespin.")
        let image2 = ProductImage(stringUrl: "https://is2-ssl.mzstatic.com/image/thumb/Video3/v4/59/11/e8/5911e892-4fcd-6827-6a0a-7c49b2cdc3d7/source/100x100bb.jpg")
        product2.images = [image2]

        
        let products: [ProductProtocol] = [product, product2]
        completion(Result.success(products))
    }
    
  
}
