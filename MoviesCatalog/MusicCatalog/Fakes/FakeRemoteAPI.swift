//
//  FakeRemoteAPI.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Result

class FakeRemoteAPI: NSObject, APIProtocol {
    
    var movieResults: [Product] = []
    
    func initialize() {}

    
    func fetchRemoteProducts(completion: @escaping ProductRequestCompletionBlock) {
        completion(Result.success(self.movieResults))
    }
}
