//
//  FakeUser.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

class FakeUser: NSObject, UserProtocol {

    static let sharedInstance = FakeUser()
}
