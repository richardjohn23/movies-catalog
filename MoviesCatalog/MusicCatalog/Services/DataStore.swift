//
//  DataStore.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 09/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Result
import Alamofire

class DataStore: NSObject, DataStoreProtocol {
    
    var movieResults: [Product] = []
    var imageResult: [ProductImage] = []
    
    func fetchProducts(completion: @escaping ProductFetchCompletionBlock) {
        
        Alamofire.request("https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all", method: .get, encoding: JSONEncoding.default).responseJSON { response in
            
            
            if let result = response.result.value {
                
                let resultDictionary = result as! NSDictionary
                
                if let movies = resultDictionary.object(forKey: "results") as! Array<NSDictionary>? {
                    
                    for movie in movies {
                        
                        let trackName = movie.value(forKey: "trackName") as? String
                        let artWork = movie.value(forKey: "artworkUrl100") as? String
                        let trackPrice = movie.value(forKey: "trackPrice") as? Double
                        let genre = movie.value(forKey: "primaryGenreName") as? String
                        let description = movie.value(forKey: "longDescription") as? String
                        
                         var product = Product(name: trackName ?? "no track name", price: trackPrice ?? 0.0, genre: genre ?? "no genre name", description: description ?? "no description")
                        let image =  ProductImage(stringUrl: artWork ?? "no image")
                        
                        product.images = [image]
                        self.movieResults.append(product)
                        
                        
                }
                    let products: [ProductProtocol] = self.movieResults
                    completion(Result.success(products))
            }
        }
    }
        
}
    
    private func saveInCoreDataWith(array: [[String: AnyObject]]) {
        
    }
        

}
