//
//  RemoteAPI.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 07/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit
import Result
import Alamofire
import CoreData

class RemoteAPI: NSObject, APIProtocol {
    
    var movieResults: [Product] = []
    var imageResult: [ProductImage] = []
    
    func initialize() {
        
    }
    
    func fetchRemoteProducts(completion: @escaping ProductRequestCompletionBlock) {
                
        Alamofire.request("https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all", method: .get, encoding: JSONEncoding.default).responseJSON { response in
            
            
            if let result = response.result.value {
                
                let resultDictionary = result as! NSDictionary
                
                if let movies = resultDictionary.object(forKey: "results") as! Array<NSDictionary>? {
                    
                    for movie in movies {
                        
                        let trackName = movie.value(forKey: "trackName") as? String
                        let artWork = movie.value(forKey: "artworkUrl100") as? String
                        let trackPrice = movie.value(forKey: "trackPrice") as? Double
                        let genre = movie.value(forKey: "primaryGenreName") as? String
                        let description = movie.value(forKey: "longDescription") as? String
                        
                        let values: Product = Product(name: trackName ?? "no track name", price: trackPrice ?? 0.0, genre: genre ?? "no genre name", description: description ?? "no description")
                        let imageValue: ProductImage = ProductImage(stringUrl: artWork ?? "no image")
                        
                        self.movieResults.append(values)
                        self.imageResult.append(imageValue)
                    }
                    
                    DispatchQueue.main.async {
                        completion(Result.success(self.movieResults))
                        
                    }
                    
                }
            }
        }
    }

}
