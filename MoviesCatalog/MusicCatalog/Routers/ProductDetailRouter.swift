//
//  ProductDetailRouter.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 07/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

protocol ProductDetailRoutingLogic {
    
}

class ProductDetailRouter: NSObject, ProductDetailRoutingLogic {
    weak var viewController: ProductDetailDisplayLogic?
}
