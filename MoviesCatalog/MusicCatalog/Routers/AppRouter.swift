//
//  AppRouter.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

class AppRouter: NSObject, AppRoutingLogic {
    
    public var controller: AppControllerProtocol!
    var appRouter: AppRoutingLogic!
    var window: UIWindow!
    var storyboard: UIStoryboard!
    
    public func moveToProducts() {
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProductsViewController")
        let navController = UINavigationController(rootViewController: viewController)
        window!.rootViewController = navController
        window!.makeKeyAndVisible()
    }

}
