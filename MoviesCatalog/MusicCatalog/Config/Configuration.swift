//
//  Configuration.swift
//  Configurations
//
//  Created by Bart Jacobs on 04/08/16.
//  Copyright © 2016 Cocoacasts. All rights reserved.
//

import UIKit

class Configuration {
    static let sharedInstance = Configuration()
    lazy var environment: Environment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            print("configuration: \(configuration)")
            let stringKey = "staging"

            if configuration.lowercased().range(of: stringKey) != nil {
                print("configuration: \(configuration)")
                return Environment.Staging
            }
        }
        
        return Environment.Production
    }()
}

enum Environment: String {
    case Staging = "staging"
    case Production = "production"

    var baseURL: String {
        switch self {
        case .Staging: return "https://staging-api.myservice.com"
        case .Production: return "https://api.myservice.com"
        }
    }

    var token: String {
        switch self {
        case .Staging: return "lktopir156dsq16sbi8"
        case .Production: return "5zdsegr16ipsbi1lktp"
        }
    }
}
