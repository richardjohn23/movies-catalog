//
//  AppController.swift
//  MusicCatalog
//
//  Created by Richard John Alamer on 06/09/2019.
//  Copyright © 2019 Richard John Alamer. All rights reserved.
//

import UIKit

class AppController: NSObject, AppControllerProtocol {
    func restAPI() {
        
    }
    
    var router: AppRoutingLogic!
    var interactor: AppBusinessLogic!
    var presenter: AppPresentationLogic!
    
    func didLaunch() {
        displayUI()
        startServices()
    }
    
    private func displayUI() {
        router.moveToProducts()
    }
    
    private func startServices() {
        interactor.initializeServices()
    }
    
    func restAPI() -> APIProtocol {
        return interactor.restAPI
    }
    
    func dataStore() -> DataStoreProtocol {
        return self.interactor.dataStore()
    }
}
